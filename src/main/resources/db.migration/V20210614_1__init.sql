drop table if exists users;
create table users(
    id serial,
    name varchar(255)
);

drop table if exists chat;
create table chat(
     id serial,
     name varchar(255)
);

drop table if exists message;
create table message(
    id serial,
    user_id bigint,
    chat_id bigint,
    text varchar(255)
);

drop table if exists model_auth;
create table model_auth(
    id serial,
    login varchar(255),
    password varchar(255),
    last_login_timestamp bigint,
    user_id bigint,
    token varchar(255)
);