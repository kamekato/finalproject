package com.example.demo.services;

import com.example.demo.models.Chat;
import com.example.demo.repositories.IChatRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ChatService {
    private IChatRepository chatRepository;


    public void addChat(Chat chat) {
        chatRepository.save(chat);
    }

    public void editChat(Chat chat){
        chat = chatRepository.findById(chat.getId()).get();
        chatRepository.save(chat);
    }

    public void deleteChat(Long id, Long name){
        Chat chat = chatRepository.findById(id).get();
        if(chat.getName().equals(name)) {
            chatRepository.deleteById(id);
        }

    }
}
