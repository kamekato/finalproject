package com.example.demo.services;

import com.example.demo.models.ModelAuth;
import com.example.demo.repositories.IModelAuthRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.UUID;

@Service
@AllArgsConstructor
public class ModelAuthService {

    private IModelAuthRepository iModelAuthRepository;

    public ModelAuth login(String login, String password) {
        ModelAuth modelAuth = iModelAuthRepository.findModelAuthByLoginAndPassword(login, password);

        modelAuth.setToken(UUID.randomUUID().toString());
        iModelAuthRepository.save(modelAuth);
        return modelAuth;
    }
}



