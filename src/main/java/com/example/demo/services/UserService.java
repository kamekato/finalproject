package com.example.demo.services;

import com.example.demo.models.User;
import com.example.demo.repositories.IUserRepository;

public class UserService {
    private IUserRepository userRepository;

    public void addUser(User user) {
       userRepository.save(user);
    }

    public void editUser(User user){
        user = userRepository.findById(user.getId()).get();
        userRepository.save(user);
    }

    public void deleteUser(Long id, Long name){
        User user = userRepository.findById(id).get();
        if(user.getName().equals(name)) {
            userRepository.deleteById(id);
        }

    }
}
