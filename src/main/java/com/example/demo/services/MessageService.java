package com.example.demo.services;

import com.example.demo.models.Message;
import com.example.demo.repositories.IMessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@AllArgsConstructor
public class MessageService {
    private IMessageRepository messageRepository;

    public void addMessage(Message message) {
        messageRepository.save(message);
    }

    public void editMessage(Message message) {
        message = messageRepository.findById(message.getId()).get();
        messageRepository.save(message);
    }

    public void deleteMessage(Long id, Long userId){
        Message message = messageRepository.findById(id).get();
        if(message.getUserId().equals(userId)) {
            messageRepository.deleteById(id);
        }
    }
}
