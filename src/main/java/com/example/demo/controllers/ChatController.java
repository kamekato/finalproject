package com.example.demo.controllers;

import com.example.demo.models.Chat;
import com.example.demo.repositories.IModelAuthRepository;
import com.example.demo.services.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/chat")
@AllArgsConstructor
public class ChatController {
    private ChatService chatService;
    private IModelAuthRepository modelAuthRepository;

    @PostMapping
    public ResponseEntity<?> addChat(@RequestBody Chat chat, @RequestHeader String token){
        chat.setName(modelAuthRepository.findByToken(token).getLogin());
        chatService.addChat(chat);
        return ResponseEntity.ok("Added");
    }

    @PutMapping
    public ResponseEntity<?> editChat(@RequestBody Chat chat, @RequestHeader String token){
        chat.setName(modelAuthRepository.findByToken(token).getLogin());
        chatService.editChat(chat);
        return ResponseEntity.ok("Edited");
    }

    @DeleteMapping("/{chatId}")
    public ResponseEntity<?> deleteChat(@PathVariable Long chatId, @RequestHeader String token){
        chatService.deleteChat(chatId, modelAuthRepository.findByToken(token).getUserId());
        return ResponseEntity.ok("Deleted");
    }


}
