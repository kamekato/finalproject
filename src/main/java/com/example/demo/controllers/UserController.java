package com.example.demo.controllers;


import com.example.demo.models.User;
import com.example.demo.repositories.IModelAuthRepository;
import com.example.demo.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
@AllArgsConstructor
public class UserController {
    private UserService userService;
    private IModelAuthRepository modelAuthRepository;

    @PostMapping
    public ResponseEntity<?> addUser(@RequestBody User user, @RequestHeader String token){
        user.setName(modelAuthRepository.findByToken(token).getLogin());
        userService.addUser(user);
        return ResponseEntity.ok("Added");
    }

    @PutMapping
    public ResponseEntity<?> editUser(@RequestBody User user, @RequestHeader String token){
        user.setName(modelAuthRepository.findByToken(token).getLogin());
        userService.editUser(user);
        return ResponseEntity.ok("Edited");
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable Long userId, @RequestHeader String token){
        userService.deleteUser(userId, modelAuthRepository.findByToken(token).getUserId());
        return ResponseEntity.ok("Deleted");
    }
}
