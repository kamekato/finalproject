package com.example.demo.controllers;

import com.example.demo.models.Message;
import com.example.demo.repositories.IModelAuthRepository;
import com.example.demo.services.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/message")
@AllArgsConstructor
public class MessageController {
    private MessageService messageService;
    private IModelAuthRepository modelAuthRepository;


    @PostMapping
    public ResponseEntity<?> addMessage(@RequestBody Message message, @RequestHeader String token){
        message.setUserId(modelAuthRepository.findByToken(token).getUserId());
        messageService.addMessage(message);
        return ResponseEntity.ok("Added");
    }

    @PutMapping
    public ResponseEntity<?> editMessage(@RequestBody Message message, @RequestHeader String token){
        message.setUserId(modelAuthRepository.findByToken(token).getUserId());
        messageService.editMessage(message);
        return ResponseEntity.ok("Edited");
    }


    @DeleteMapping("/{messageId}")
    public ResponseEntity<?> deleteMessage(@PathVariable Long messageId, @RequestHeader String token){
        messageService.deleteMessage(messageId, modelAuthRepository.findByToken(token).getUserId());
        return ResponseEntity.ok("Deleted");
    }

}
