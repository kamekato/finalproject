package com.example.demo.repositories;

import com.example.demo.models.Chat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IChatRepository extends JpaRepository<Chat,Long> {
}
