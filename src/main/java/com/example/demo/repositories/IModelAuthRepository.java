package com.example.demo.repositories;

import com.example.demo.models.ModelAuth;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IModelAuthRepository extends JpaRepository<ModelAuth, Long> {
    ModelAuth findModelAuthByLoginAndPassword (String login, String password);

    ModelAuth findByToken(String token);
}
