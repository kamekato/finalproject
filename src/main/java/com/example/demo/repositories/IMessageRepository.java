package com.example.demo.repositories;


import com.example.demo.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IMessageRepository extends JpaRepository<Message, Long> {

}
